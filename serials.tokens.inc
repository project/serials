<?php

/**
 * @file
 * serials.tokens.inc
 */

use Drupal\Core\Render\BubbleableMetadata;

/**
 * Implements hook_token_info().
 */
function serials_token_info() {
  return [
    'tokens' => [
      // Type.
      'site' => [
        // Token.
        'serials' => [
          'name' => t('Get serials per key'),
          'description' => t('Use: [site:serials:foo-bar-baz].'),
        ],
      ],
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function serials_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $tokenService = \Drupal::token();
  /** @var \Drupal\serials\SerialsServiceFactoryInterface $serialsFactory */
  $serialsFactory = \Drupal::service('serials.factory');

  $replacements = [];
  if ($type == 'site') {
    foreach ($tokenService->findWithPrefix($tokens, 'serials') as $key => $original) {
      $replacements[$original] = $serialsFactory->serialsService($key)->nextId();
    }
  }
  return $replacements;
}
