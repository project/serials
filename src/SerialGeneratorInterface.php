<?php

namespace Drupal\serials;

/**
 * An efficient, reusable, thread-safe serial generator.
 */
interface SerialGeneratorInterface {

  /**
   * Generate a serial.
   *
   * @return int
   *   The value.
   */
  public function generate();

  /**
   * Reset a serial.
   *
   * @param int $value
   *   The value.
   * @param bool $force
   *   Force setting values to a smaller value, risking duplicate serials.
   *   Defaults to false.
   *
   * @see \Drupal\serials\SerialsGeneratorInterface::resetMultiple
   */
  public function reset(int $value, bool $force = FALSE);

}
