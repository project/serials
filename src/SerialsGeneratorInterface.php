<?php

namespace Drupal\serials;

/**
 * An efficient, reusable, thread-safe multi-serials generator.
 */
interface SerialsGeneratorInterface {

  /**
   * Generate multiple serials.
   *
   * @param array $keys
   *   The keys.
   *
   * @return int[]
   *   The serials.
   */
  public function generateMultiple(array $keys): array;

  /**
   * Generate one serial.
   *
   * @param string $key
   *   The key.
   *
   * @return int
   *   The value.
   */
  public function generate(string $key): int;

  /**
   * Reset multiple serials.
   *
   * Ensure that a values for the keys exist, and if they exist already, that
   * further values are greater than the passed ones. Existing values are only
   * decreased if the $force parameter is set.
   * This is a code path that is used only once per key so this does not need
   * much optimization.
   *
   * @param int[] $keyedValues
   *   An array of key-value pairs.
   * @param bool $force
   *   Force setting values to a smaller value, risking duplicate serials.
   *   Defaults to false.
   */
  public function resetMultiple(array $keyedValues, bool $force = FALSE);

  /**
   * Reset one serial.
   *
   * @param string $key
   *   The key.
   * @param int $value
   *   The value.
   * @param bool $force
   *   Force setting values to a smaller value, risking duplicate serials.
   *   Defaults to false.
   *
   * @see \Drupal\serials\SerialsGeneratorInterface::resetMultiple
   */
  public function reset(string $key, int $value, bool $force = FALSE);

}
